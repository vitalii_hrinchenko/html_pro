(function () {
    var photo_1 = document.querySelector('.photo_1');

    var observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (typeof getCurrentAnimationPreference === 'function' && !getCurrentAnimationPreference()) {
                return;
            }

            if (entry.isIntersecting) {
                entry.target.classList.add('photo_1_768');
            }
        });
    });

    observer.observe(photo_1);
})();

(function () {
    var photo_3 = document.querySelector('.photo_3');

    var observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (typeof getCurrentAnimationPreference === 'function' && !getCurrentAnimationPreference()) {
                return;
            }

            if (entry.isIntersecting) {
                entry.target.classList.add('photo_3_768');
            }
        });
    });

    observer.observe(photo_3);
})();

(function () {
    var photo_4 = document.querySelector('.photo_4');

    var observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (typeof getCurrentAnimationPreference === 'function' && !getCurrentAnimationPreference()) {
                return;
            }

            if (entry.isIntersecting) {
                entry.target.classList.add('photo_4_768');
            }
        });
    });

    observer.observe(photo_4);
})();

(function () {
    var photo_5 = document.querySelector('.photo_5');

    var observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (typeof getCurrentAnimationPreference === 'function' && !getCurrentAnimationPreference()) {
                return;
            }

            if (entry.isIntersecting) {
                entry.target.classList.add('photo_5_768');
            }
        });
    });

    observer.observe(photo_5);
})();

(function () {
    var photo_6 = document.querySelector('.photo_6');

    var observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (typeof getCurrentAnimationPreference === 'function' && !getCurrentAnimationPreference()) {
                return;
            }

            if (entry.isIntersecting) {
                entry.target.classList.add('photo_6_768');
            }
        });
    });

    observer.observe(photo_6);
})();

(function () {
    var photo_2 = document.querySelector('.photo_2');

    var observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (typeof getCurrentAnimationPreference === 'function' && !getCurrentAnimationPreference()) {
                return;
            }

            if (entry.isIntersecting) {
                entry.target.classList.add('photo_2_768');
            }
        });
    });

    observer.observe(photo_2);
})();